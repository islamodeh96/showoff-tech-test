# Showoff Tech

This is a simple Ruby on Rails app that communicats with Showoff tech API V1 endpoints.

##### Prerequisites

The setups steps expect following tools installed on the system.
- Ruby [2.6.3]
- Rails [5.2]

##### Demo

http://showoff-me.herokuapp.com

##### 1. Check out the repository

```bash
git clone git@bitbucket.org:islamodeh96/showoff-tech-test.git
```

##### 2. Copy .env.sample file

Copy the sample .env.sample to .env file and edit the environment configuration as required.

```bash
cp .env.sample .env
```

##### 3. Install required gems

```bash
bundle exec bundle i
```

##### 4. Start the Rails server

You can start the rails server using the command given below.

```ruby
bundle exec rails s
```

And now you can visit the site with the URL http://localhost:3000

