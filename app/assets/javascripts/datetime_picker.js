$(document).on('turbolinks:load', function() {
  // render flatpickr for all input dates with the class of flatpickr
  $("form input[type='text'].flatpickr").map(function() {
    let numeric_date = parseInt($(this).val()) || 946677600000
    let fp = $(this).flatpickr({ altInput: true, altFormat: "F j, Y", defaultDate: new Date(numeric_date), maxDate: new Date() })

    $(this).parents().find("form").submit( () => {
      // convert all dates to numeric before submitting the form.
      $(this).val(Date.parse(fp.latestSelectedDateObj))
      return true
    })
  })

  $("span.flatpickr").map(function() {
    numeric_date = parseInt($(this).html())

    if (!isNaN(numeric_date)){
      date = (new Date(numeric_date)).toDateString()
      $(this).html(date)
    }
  })
})
