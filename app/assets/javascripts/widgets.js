function widgetEditModal(widget) {
  data = {
    widget_name: widget.name,
    widget_description: widget.description,
  }

  // parse html form content to the edit modal 
  $("#edit_widget_modal .modal-body").html(Mustache.render(window.widget_mustache_form_template, data))
  // mustache can't determine what's the value for the select tag.
  $("#mustache_form").find("select[name='widget_vm[kind]']").find(`option[value='${widget.kind}']`).attr("selected", true)
  // set the id correclty since its empty (Rails raise error for bad URI)
  $("#mustache_form form").attr("action", "/widgets/" + widget.id)
  // open modal
  $("#edit_widget_modal").modal("show")
}