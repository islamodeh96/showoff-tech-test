class ApplicationController < ActionController::Base
  include SessionConcerns
  helper_method :logged_in?
  helper_method :current_user

  rescue_from UserVm::NotAuthorized, with: :deny_access

  private

  def deny_access(e)
    flash[:danger] = "Token expired.. Please login again" if refresh_token.blank?
    redirect_to root_path
  end
end
