module AuthenticatorConcerns
  extend ActiveSupport::Concern

  included do
    before_action :authenticate
  end

  def authenticate
    if !logged_in?
      flash[:danger] = "Please login first"
      redirect_to root_path
    end
  end
end
