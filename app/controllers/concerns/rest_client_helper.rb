module RestClientHelper
  require 'ostruct'

  class << self
    def get(url, payload={}, headers={})
      processor(url, payload, headers, :get)
    end

    def post(url, payload={}, headers= { content_type: :json, accept: :json })
      processor(url, payload, headers, :post)
    end

    def put(url, payload={}, headers= { content_type: :json, accept: :json })
      processor(url, payload, headers, :put)
    end

    def delete(url, payload={}, headers= { content_type: :json, accept: :json })
      processor(url, payload, headers, :delete)
    end

    private

    def processor(url, payload, headers, method)
      response = nil

      case method
      when :get, :delete
        url = Faraday::Connection.new(url: url, params: payload).build_url.to_s

        response = RestClient.send(method, url, headers) { |response, request, result, &block|
          OpenStruct.new({ code: response.code, body: (JSON.parse(response.body) rescue nil) } )
        }
      else
        response = RestClient.send(method, url, payload.to_json, headers = headers) { |response, request, result, &block|
          OpenStruct.new({ code: response.code, body: (JSON.parse(response.body) rescue nil) } )
        }
      end

      raise UserVm::NotAuthorized if response.code == 401

      response
    end
  end
end
