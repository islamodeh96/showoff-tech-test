module SessionConcerns
  extend ActiveSupport::Concern

  def current_user
    @current_user ||= UserVm.new(session[:user_vm_attributes])
  end

  def login(user_vm)
    email = user_vm.email
    password = user_vm.password
    end_point_url = API_V1_BASE_URL + "/oauth/token"

    payload = {
      grant_type: "password",
      client_id: API_V1_CLIENT_ID,
      client_secret: API_V1_CLIENT_SECRET,
      username: email,
      password: password
    }

    response = RestClientHelper.post(end_point_url, payload)

    case response.code
    when 200
      session[:token] = response.body["data"]["token"]
      flash[:success] = response.body["message"]
      session[:user_vm_attributes] = load_user_vm_attributes
      true
    else
      flash[:danger] = response.body["message"]
      false
    end
  end

  def logout
    end_point_url = API_V1_BASE_URL + "/oauth/revoke"

    payload = {
      token: access_token
    }

    headers = {
      content_type: "application/json",
      Authorization: authorization_bearer
    }

    response = RestClientHelper.post(end_point_url, payload, headers)

    case response.code
    when 200
      flash[:success] = "Logged out"
      reset_session
      true
    else
      flash[:danger] = response.body["message"]
      reset_session
      false
    end
  end

  def authorization_bearer
    "Bearer #{ access_token}" if access_token.present?
  end

  def access_token
    session[:token]["access_token"] if session[:token].present?
  end

  def logged_in?
    !token_expired?
  end

  private

  def refresh_token_value
    session[:token]["refresh_token"] if session[:token].present?
  end

  def token_expired?
    created_at = session[:token].try(:[], "created_at")
    expires_in = session[:token].try(:[], "expires_in")

    if created_at.present? && expires_in.present?
      if (DateTime.now > Time.at(created_at + expires_in.second))
        refresh_token
      else
        false
      end
    else
      true
    end
  end

  def refresh_token
    end_point_url = API_V1_BASE_URL + "/oauth/token"

    payload = {
      grant_type: "refresh_token",
      refresh_token: refresh_token_value,
      client_id: API_V1_CLIENT_ID,
      client_secret: API_V1_CLIENT_SECRET,
    }

    response = RestClientHelper.post(end_point_url, payload)

    case response.code
    when 200
      session[:token] = response.body["data"]["token"]
      true
    else
      # reset session if couldn't refresh the token
      reset_session
      false
    end
  end

  def load_user_vm_attributes
    end_point_url = API_V1_BASE_URL + "/api/v1/users/me"
    payload = {}

    headers = {
      Authorization: authorization_bearer
    }

    response = RestClientHelper.get(end_point_url, payload, headers)
    case response.code
    when 200
      response.body["data"]["user"]
    else
      {}
    end
  end
end
