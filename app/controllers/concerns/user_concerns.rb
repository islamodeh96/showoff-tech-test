module UserConcerns
  extend ActiveSupport::Concern

  def create_user(user_vm)
    email = user_vm.email
    password = user_vm.password
    end_point_url = API_V1_BASE_URL + "/api/v1/users"

    payload = {
      grant_type: "password",
      client_id: API_V1_CLIENT_ID,
      client_secret: API_V1_CLIENT_SECRET,
      user: user_vm.as_json.with_indifferent_access.slice(:email, :password, :first_name, :last_name, :image_url, :date_of_birth)
    }

    response = RestClientHelper.post(end_point_url, payload)

    case response.code
    when 200
      flash[:success] = response.body["message"]
      login(user_vm)
      true
    else
      flash[:danger] = response.body["message"]
      false
    end
  end

  def update_user(user_vm)
    end_point_url = API_V1_BASE_URL + "/api/v1/users/#{current_user.id}"

    payload = {
      client_id: API_V1_CLIENT_ID,
      client_secret: API_V1_CLIENT_SECRET,
      user: user_vm.as_json.with_indifferent_access.slice(:email, :first_name, :last_name, :image_url, :date_of_birth)
    }

    headers = {
      content_type: "application/json",
      Authorization: authorization_bearer
    }

    response = RestClientHelper.put(end_point_url, payload, headers)

    case response.code
    when 200
      flash[:success] = response.body["message"]
      session[:user_vm_attributes] = load_user_vm_attributes
      true
    else
      flash[:danger] = response.body["message"]
      false
    end
  end

  def show_user(user_vm)
    end_point_url = API_V1_BASE_URL + "/api/v1/users/#{user_vm.id}"
    headers = {
      content_type: "application/json",
      Authorization: authorization_bearer
    }

    response = RestClientHelper.get(end_point_url, {}, headers)

    case response.code
    when 200
      UserVm.new(response.body["data"]["user"])
    else
      flash[:danger] = response.body["message"]
      UserVm.new
    end
  end

  def user_change_password(user_vm)
    end_point_url = API_V1_BASE_URL + "/api/v1/users/me/password"
    payload = {
      user: {
        current_password: user_vm.old_password,
        new_password: user_vm.password
      }
    }

    headers = {
      content_type: "application/json",
      Authorization: authorization_bearer
    }

    response = RestClientHelper.post(end_point_url, payload, headers)

    case response.code
    when 200
      flash[:success] = response.body["message"]
      true
    else
      flash[:danger] = response.body["message"]
      false
    end
  end

  def user_reset_password(user_vm)
    email = user_vm.email
    end_point_url = API_V1_BASE_URL + "/api/v1/users/reset_password"

    payload = {
      client_id: API_V1_CLIENT_ID,
      client_secret: API_V1_CLIENT_SECRET,
      user: {
        email: user_vm.email
      }
    }

    response = RestClientHelper.post(end_point_url, payload)

    case response.code
    when 200
      flash[:success] = response.body["message"]
      true
    else
      flash[:danger] = response.body["message"]
      false
    end
  end

  def user_check_email(user_vm)
    end_point_url = API_V1_BASE_URL + "/api/v1/users/email"

    payload = {
      client_id: API_V1_CLIENT_ID,
      client_secret: API_V1_CLIENT_SECRET,
      email: user_vm.email
    }

    response = RestClientHelper.get(end_point_url, payload, headers)

    case response.code
    when 200
      if response.body["data"]["available"].present?
        flash[:success] = "Email #{user_vm.email} is available"
      else
        flash[:warning] = "Email #{user_vm.email} is not available"
      end

      true
    else
      flash[:danger] = response.body["message"]
      false
    end
  end
end