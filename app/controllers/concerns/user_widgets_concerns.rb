module UserWidgetsConcerns
  def user_id_widgets(user_id, term)
    end_point_url = API_V1_BASE_URL + "/api/v1/users/#{user_id}/widgets"
    payload = {
      client_id: API_V1_CLIENT_ID,
      client_secret: API_V1_CLIENT_SECRET,
      term: term,
    }

    headers = {
      Authorization: authorization_bearer
    }

    response = RestClientHelper.get(end_point_url, payload, headers)

    case response.code
    when 200
      WidgetVm.initialize_array(response.body["data"]["widgets"])
    else
      flash[:danger] = response.body["message"]
      []
    end
  end
end