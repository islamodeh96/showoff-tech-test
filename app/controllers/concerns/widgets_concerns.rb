module WidgetsConcerns
  def all_widgets
    end_point_url = API_V1_BASE_URL + "/api/v1/widgets"
    headers = {
      Authorization: authorization_bearer
    }

    response = RestClientHelper.get(end_point_url, {}, headers)

    case response.code
    when 200
      WidgetVm.initialize_array(response.body["data"]["widgets"])
    else
      flash[:danger] = response.body["message"]
      []
    end
  end

  def visible_widgets(term=nil)
    end_point_url = API_V1_BASE_URL + "/api/v1/widgets/visible"

    payload = {
      client_id: API_V1_CLIENT_ID,
      client_secret: API_V1_CLIENT_SECRET,
      term: term
    }

    response = RestClientHelper.get(end_point_url, payload, headers)

    case response.code
    when 200
      WidgetVm.initialize_array(response.body["data"]["widgets"])
    else
      flash[:danger] = response.body["message"]
      []
    end
  end

  def create_widget(widget_vm)
    end_point_url = API_V1_BASE_URL + "/api/v1/widgets"

    payload = {
      widget: {
        name: widget_vm.name,
        description: widget_vm.description,
        kind: widget_vm.kind
      }
    }

    headers = {
      content_type: "application/json",
      Authorization: authorization_bearer
    }

    response = RestClientHelper.post(end_point_url, payload, headers)

    case response.code
    when 200
      flash[:success] = response.body["message"]
      true
    else
      flash[:danger] = response.body["message"]
      false
    end
  end

  def update_widget(widget_vm)
    end_point_url = API_V1_BASE_URL + "/api/v1/widgets/#{widget_vm.id}"
    payload = {
      widget: {
        name: widget_vm.name,
        description: widget_vm.description,
        kind: widget_vm.kind
      }
    }

    headers = {
      content_type: "application/json",
      Authorization: authorization_bearer
    }

    response = RestClientHelper.put(end_point_url, payload, headers)

    case response.code
    when 200
      flash[:success] = response.body["message"]
      true
    else
      flash[:danger] = response.body["message"]
      false
    end
  end

  def destroy_widget(widget_vm)
    end_point_url = API_V1_BASE_URL + "/api/v1/widgets/#{widget_vm.id}"
    headers = {
      content_type: "application/json",
      Authorization: authorization_bearer
    }

    response = RestClientHelper.delete(end_point_url, {}, headers)

    case response.code
    when 200
      flash[:success] = response.body["message"]
      true
    else
      flash[:danger] = response.body["message"]
      false
    end
  end
end
