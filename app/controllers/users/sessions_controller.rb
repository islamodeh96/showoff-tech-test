class Users::SessionsController < ApplicationController
  before_action -> { redirect_to user_widgets_path(current_user.try(:id)) }, if: :logged_in?, only: [:new, :create]

  def new
    @user_vm = UserVm.new
  end

  def create
    if login(UserVm.new(user_params_require))
      redirect_to user_widgets_path(:me)
    else
      redirect_to root_path
    end
  end

  def destroy
    if logout
      flash[:success] = "Logged out successfully."
    end

    redirect_to root_path
  end

  def reset_passwords
    reset_password(UserVm.new(user_params_require))
    redirect_to root_path
  end

  private

  def user_params_require
    params.require(:user_vm).permit(:email, :password)
  end
end
