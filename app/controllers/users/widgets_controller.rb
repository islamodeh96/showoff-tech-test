class Users::WidgetsController < ApplicationController
  include AuthenticatorConcerns
  include UserWidgetsConcerns

  def index
    @user_id  = params[:user_id]
    @all_widgets = user_id_widgets(@user_id, params[:term])
  end
end
