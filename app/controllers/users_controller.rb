class UsersController < ApplicationController
  include AuthenticatorConcerns
  include UserConcerns
  skip_before_action :authenticate, only: [:create, :reset_password, :check_email]

  def create
    @user_vm = UserVm.new(user_params_require)
    if @user_vm.valid?
      create_user(@user_vm)
    else
      flash[:danger] = @user_vm.errors.full_messages.join(", ")
    end

    redirect_to root_path
  end

  def update
    @user_vm = UserVm.new(user_params_require)

    if @user_vm.valid?
      update_user(@user_vm)
    else
      flash[:danger] = @user_vm.errors.full_messages.join(", ")
    end

    redirect_to user_path(current_user.try(:id))
  end

  def show
    @user_vm = show_user(UserVm.new(id: params[:id]))
  end

  def reset_password
    user_reset_password(UserVm.new(email: params[:user_vm][:email]))
    redirect_to root_path
  end

  def change_password
    user_vm = current_user
    user_vm.old_password = params[:user_vm][:old_password]
    user_vm.password = params[:user_vm][:password]
    user_vm.password_confirmation = params[:user_vm][:password_confirmation]

    if user_vm.valid?
      user_change_password(user_vm)
    else
      flash[:danger] = user_vm.errors.full_messages.join(", ")
    end

    redirect_to user_widgets_path(current_user.try(:id))
  end

  def check_email
    @user_vm = UserVm.new(email: params[:user_vm].try(:[], :email))
    user_check_email(@user_vm) if @user_vm.email.present?
  end

  private

  def user_params_require
    params.require(:user_vm).permit!
  end
end
