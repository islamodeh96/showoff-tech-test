class WidgetsController < ApplicationController
  include AuthenticatorConcerns
  include WidgetsConcerns
  skip_before_action :authenticate, only: :visible

  def index
    @all_widgets = all_widgets
  end

  def new
    @widget_vm = WidgetVm.new
  end

  def create
    @widget_vm = WidgetVm.new(widget_params_require)
    if @widget_vm.valid?
      if create_widget(@widget_vm)
        redirect_to user_widgets_path(current_user.id)
        return
      end
    else
      flash[:danger] = @widget_vm.errors.full_messages.join(", ")
    end

    redirect_to new_widget_path
  end

  def update
    widget_vm = WidgetVm.new(widget_params_require)
    widget_vm.id = params[:id]

    if widget_vm.valid?
      update_widget(widget_vm)
    else
      flash[:danger] = widget_vm.errors.full_messages.join(", ")
    end

    redirect_to widgets_path
  end

  def destroy
    widget_vm = WidgetVm.new(id: params[:id])
    destroy_widget(widget_vm)
    redirect_to user_widgets_path(:me)
  end

  def visible
    @all_widgets = visible_widgets(params[:term])
  end

  private

  def widget_params_require
    params.require(:widget_vm).permit(:name, :description, :kind)
  end
end
