class UserVm < Vm
  class NotAuthorized < StandardError; end

  attr_accessor :id, :name, :images, :date_of_birth, :active, :email, :old_password, :password, :password_confirmation, :first_name, :last_name, :image_url
  validate :password_confirmation_match
  validates :email, :first_name, :last_name, presence: :true

  private

  def password_confirmation_match
    errors.add(:password, "Password & Password confirmation doesn't match") if password != password_confirmation
    errors.blank?
  end
end
