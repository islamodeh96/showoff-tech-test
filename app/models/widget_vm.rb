class WidgetVm < Vm
  KINDS = ["visible", "hidden"].freeze

  attr_accessor :id, :name, :description, :kind, :user, :owner
  validates :name, :description, :kind, presence: :true
  validates :kind, inclusion: { in: KINDS }

  def self.initialize_array(attributes=[])
    attributes.map do |attribute|
      WidgetVm.new(attribute)
    end
  end
end
