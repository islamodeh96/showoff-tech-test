Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :users do
    post :reset_password
    post :change_password
    get :check_email

    resource :sessions, only: [:new, :create, :destroy] do
    end
  end

  resources :users, only: [:show, :create, :update] do
    resources :widgets, only: [:index], controller: "users/widgets"
  end

  resources :widgets, only: [:index, :new, :create, :update, :destroy]

  match "widgets/visible" => "widgets#visible", :via => :get
  root to: 'widgets#visible'
end
