require 'rails_helper'

RSpec.describe UserVm, type: :model do
  subject { UserVm.new }

  describe "Inheritance" do
    it "Should be inherited from Vm class" do
      expect(described_class.superclass).to eq Vm
    end
  end

  describe "Attributes" do
    attributes = [:id, :name, :images, :date_of_birth, :active, :email, :old_password, :password, :password_confirmation, :first_name, :last_name, :image_url]
    attributes.each do |attr|
      it { should have_attr_accessor(attr) }      
    end
  end

  describe "Validations" do
    context "Presence" do
      attributes = [:email, :first_name, :last_name]
      attributes.each do |attr|
         it { should validate_presence_of(attr) }
      end
    end

    context "Passowrd" do
      it "Should validate that password and password_confirmation are equal" do
        subject.password = "bla"
        subject.password_confirmation = "bla"
        subject.valid?
        expect(subject.errors[:password]).to be_blank
        subject.password_confirmation = "bla123"
        subject.valid?
        expect(subject.errors[:password]).to_not be_blank
      end
    end
  end
end
