require 'rails_helper'

RSpec.describe Vm, type: :model do
  subject { Vm.new }

  describe "Modules" do
    it "Should include ActiveRecord::Model" do
      expect(described_class.ancestors).to include(ActiveModel::Model)
    end

    it "Should include ActiveModel::Validations" do
      expect(described_class.ancestors).to include(ActiveModel::Validations)
    end
  end
end
