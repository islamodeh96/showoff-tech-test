require 'rails_helper'

RSpec.describe WidgetVm, type: :model do
  subject { WidgetVm.new }

  describe "Inheritance" do
    it "Should be inherited from Vm class" do
      expect(described_class.superclass).to eq Vm
    end
  end

  describe "Attributes" do
    attributes = [:id, :name, :description, :kind, :user, :owner]
    attributes.each do |attr|
      it { should have_attr_accessor(attr) }      
    end
  end

  describe "Validations" do
    context "Presence" do
      attributes = [:name, :description, :kind]
      attributes.each do |attr|
         it { should validate_presence_of(attr) }
      end
    end

    context "Inlcusion" do
      it "Should validate that kind attribute is in KINDS variable" do
        expect(subject).to validate_inclusion_of(:kind).in_array(WidgetVm::KINDS)
      end
    end

    context "Variables" do
      it "Kind variable must include visible & hidden values" do
        expect(WidgetVm::KINDS).to eq ["visible", "hidden"]
      end
    end
  end

  describe "Class methods" do
    context "Initialize Array" do
      it "Should return array of WidgetVm models if the passed attribute is array of hashes" do
        widget_vms = WidgetVm.initialize_array([{id: 1}, {id: 2}])
        expect(widget_vms.count).to eq 2
        expect(widget_vms.map(&:id)).to eq [1,2]
        expect(widget_vms.first.class).to eq WidgetVm
      end
    end
  end
end
